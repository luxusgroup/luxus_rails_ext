String.class_eval do
	def is_numeric?
		true if Float(self) rescue false
	end

	def is_boolean?
		self.downcase == 'true' || self.downcase == 'false'
	end
end
