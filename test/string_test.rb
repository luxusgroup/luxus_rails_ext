require 'test_helper'

class StringTest < ActiveSupport::TestCase
	def test_is_numeric
		assert_equal true, '123456'.is_numeric?
		assert_equal false, 'a1'.is_numeric?
	end

	def test_is_bool
		assert_equal true, 'true'.is_boolean?
		assert_equal true, 'tRuE'.is_boolean?
		assert_equal true, 'false'.is_boolean?
		assert_equal true, 'fAlSe'.is_boolean?
		assert_equal false, ''.is_boolean?
		assert_equal false, 'abc'.is_boolean?
	end
end
