$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'luxus_rails_ext/version'

# Describe your gem and declare its dependencies:
# TODO: need to make sure home page, summary, & description are filled out
Gem::Specification.new do |s|
  s.name = 'luxus_rails_ext'
  s.version     = LuxusRailsExt::VERSION
  s.authors = ['Dan Nelson']
  s.email = ['dan@luxusgroup.com']
  s.homepage = 'https://bitbucket.org/luxusgroup/luxus_rails_ext'
  s.summary = 'Luxus Rails Extensions'
  s.description = 'Luxus Rails Extensions'
  s.license = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'rails', '~> 4.2.0.rc2'

  s.add_development_dependency 'sqlite3'
end
